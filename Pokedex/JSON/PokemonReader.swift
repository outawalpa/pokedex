//
//  PokemonReader.swift
//  Pokedex
//
//  Created by Ob'dO Contact Agile on 30/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation

// https://pokeapi.co/api/v2/pokemon/ 
struct Pokemon: Decodable {
    let results: [PokemonResults]
}

struct PokemonResults: Decodable {
    let name: String
    let url: String
}


// https://pokeapi.co/api/v2/type/
struct Type: Decodable {
    let results: [TypeResults]
}

struct TypeResults: Decodable {
    let name: String
    let url: String
}


struct Generation: Decodable {
    let results: [GenerationResults]
}

struct GenerationResults: Decodable {
    let name: String
}
