//
//  Extension.swift
//  Pokedex
//
//  Created by Ob'dO Contact Agile on 30/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    /**
     Round the border
    */
    public func round() {
        self.layer.cornerRadius = 15
    }
}
