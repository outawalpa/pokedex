//
//  ViewController.swift
//  Pokedex
//
//  Created by Ob'dO Contact Agile on 30/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import UIKit
import Spring
import Alamofire

class PokemonListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var screen: UIView!
    @IBOutlet weak var pokemonList: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingImage: SpringImageView!
    @IBOutlet weak var searchButton: SpringButton!
    @IBOutlet weak var filterButton: SpringButton!
    
    var allPokemon: [String] = []{
        didSet {
            filterPokemon = self.allPokemon
        }
    }
    
    var allType: [String] = []
    var allGeneration: [String] = []
    
    var filterPokemon: [String] = [] {
        didSet {
            pokemonList.reloadData()
        }
    }
    
    var search = 0
    var loadingImageTimer: Timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchButton.round()
        filterButton.round()
        screen.round()
        pokemonList.round()
        loadingView.round()
        
        //Request
        loadingView.isHidden = false
        loadingImageTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.animation), userInfo: nil, repeats: true)
        searchAllPokemon()
        searchAllType()
        searchAllGeneration()
        
        //Table
        let pokemonListTableNib = UINib(nibName: PokemonListTableViewCell.NibName, bundle: nil)
        pokemonList.register(pokemonListTableNib, forCellReuseIdentifier: PokemonListTableViewCell.Identifier)
        
        pokemonList.delegate = self
        pokemonList.dataSource = self
    }

    //Table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PokemonListTableViewCell.Identifier, for: indexPath) as! PokemonListTableViewCell

        cell.pokemonName.text = filterPokemon[indexPath.row]
        
        let pokemonNumber: String = convertNumber(allPokemon.firstIndex(of: filterPokemon[indexPath.row])!)
        cell.pokemonNumber.text = pokemonNumber
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    
    //Utils
    func convertNumber(_ number: Int) -> String {
        if number < 9 {
            return "00\(number+1)"
        }
        else if number < 99 {
            return "0\(number+1)"
        }
        else {
            return "\(number+1)"
        }
    }
    
    
    //MARK - Request
    
    //All Pokemon
    func searchAllPokemon() {
        
        self.search += 1
        
        DispatchQueue.main.async {
            let url = API().API_URL + "pokemon?limit=-1)"
            
            Alamofire.request(url, method: .get).responseJSON { response in
                if response.result.isSuccess {
                    guard let data = response.data else { return }
                    
                    do {
                        let myResponse = try JSONDecoder().decode(Pokemon.self, from: data)
                        for result in myResponse.results {
                            self.allPokemon.append(result.name)
                        }
                        self.pokemonList.reloadData()
                        self.search -= 1
                        
                        if self.search == 0 {
                            self.loadingView.isHidden = true
                            self.loadingImageTimer.invalidate()
                        }
                    }
                    catch{
                        //TODO
                        print("catch")
                    }
                }
                else {
                    //TODO
                    print("isFailure")
                }
            }
        }
    }
    
    //All type
    func searchAllType() {
        
        self.search += 1
        
        DispatchQueue.main.async {
            let url = API().API_URL + "type?limit=-1)"
            
            Alamofire.request(url, method: .get).responseJSON { response in
                if response.result.isSuccess {
                    guard let data = response.data else { return }
                    
                    do {
                        let myResponse = try JSONDecoder().decode(Type.self, from: data)
                        for result in myResponse.results {
                            self.allType.append(result.name)
                        }
                        self.search -= 1
                        
                        if self.search == 0 {
                            self.loadingView.isHidden = true
                            self.loadingImageTimer.invalidate()
                        }
                    }
                    catch{
                        //TODO
                        print("catch")
                    }
                }
                else {
                    //TODO
                    print("isFailure")
                }
            }
        }
    }
    
    //All type
    func searchAllGeneration() {
        
        self.search += 1
        
        DispatchQueue.main.async {
            let url = API().API_URL + "generation?limit=-1)"
            
            Alamofire.request(url, method: .get).responseJSON { response in
                if response.result.isSuccess {
                    guard let data = response.data else { return }
                    
                    do {
                        let myResponse = try JSONDecoder().decode(Generation.self, from: data)
                        for result in myResponse.results {
                            self.self.allGeneration.append(result.name)
                        }

                        self.search -= 1
                        
                        if self.search == 0 {
                            self.loadingView.isHidden = true
                            self.loadingImageTimer.invalidate()
                        }
                    }
                    catch{
                        //TODO
                        print("catch")
                    }
                }
                else {
                    //TODO
                    print("isFailure")
                }
            }
        }
    }
    
    @objc func animation()
    {
        self.loadingImage.animation = "swing"
        self.loadingImage.curve = "easeOut"
        self.loadingImage.duration = 1
        self.loadingImage.animate()
    }
}

