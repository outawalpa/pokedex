//
//  PokemonListTableViewCell.swift
//  Pokedex
//
//  Created by Ob'dO Contact Agile on 30/08/2019.
//  Copyright © 2019 CarlDelacour. All rights reserved.
//

import UIKit

class PokemonListTableViewCell: UITableViewCell {

    @IBOutlet weak var pokemonNumber: UILabel!
    @IBOutlet weak var pokemonName: UILabel!
    
    // MARK: - Static attributes
    static let NibName = "PokemonListTableViewCell"
    static let Identifier = "pokemonListCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
